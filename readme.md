4 tests have been created for testing REST-api. 
2 positive:
 - SaveAndDeleteAuthor. Verify possibility of creating and deleting created author by using 
POST and DELETE methods.
- AuthorNameFromResponse. It's initial test for playing with RestAssured. Initial 
author with specified id has been created. After that by using Get method, test verify
whether response has the correct author name.

2 negative:
 - CreateAuthorWithExistedId. Verify whether correct status code (409) will be received
 if user try to create an author with existed ID.
 - WrongSizeQueryParameter. Test to play with query parameter. Size for list of authors
 must be positive number. Test verify that errorMessage and error are correct in respone, when user
 enter negative value as size parameter.
