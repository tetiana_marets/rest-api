package positive;

import com.epam.ta.rest.constants.Constants;
import com.epam.ta.rest.model.author.Author;
import com.epam.ta.rest.service.AuthorService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SaveAndDeleteAuthor {
    private static final Logger log = LogManager.getLogger(SaveAndDeleteAuthor.class);
    AuthorService authorService;

    @BeforeTest
    public void setUp() {
        authorService = new AuthorService();
    }

    @Test
    public void createAndDeleteAuthor(){
        AuthorService authorService = new AuthorService();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        Author author = authorService.createAuthorEntity();
        log.info("Trying to create author: " + author.toString());
        String jsonString = gson.toJson(author);
        log.info(jsonString);
        log.info(Constants.BASE_URI+Constants.AUTHOR_ENDPOINT);
        authorService.createAuthor(jsonString).then()
                .assertThat()
                .statusCode(201);
        log.info("Save new author with id: " + author.getAuthorId());
        //deleting created author
        authorService.deleteAuthor(author.getAuthorId()).then()
                .assertThat()
                .statusCode(204);
        log.info("Delete author with id: " + author.getAuthorId());
    }
}
