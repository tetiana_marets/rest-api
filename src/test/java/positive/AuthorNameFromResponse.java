package positive;

import com.epam.ta.rest.constants.Constants;
import com.epam.ta.rest.model.author.Author;
import com.epam.ta.rest.service.AuthorService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;
import static org.hamcrest.Matchers.equalTo;

public class AuthorNameFromResponse {
    private static final Logger log = LogManager.getLogger(AuthorNameFromResponse.class);
    AuthorService authorService;
    Author author;

    @BeforeTest
    public void setUp(){
        authorService = new AuthorService();
        author = authorService.createAuthorEntity();
        log.info("Trying to create author: " + author.toString());
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        String jsonString = gson.toJson(author);
        if (authorService.createAuthor(jsonString).getStatusCode() == 200) {
            ;
            log.info("Created author with ID and last name: "
                    + author.getAuthorId()
                    + author.getAuthorName().getSecond());
        }
        else
        {}
    }

    @Test
    public void authorNameFromResponse(){
        get(Constants.AUTHOR_ENDPOINT + author.getAuthorId())
                .then()
                .assertThat()
                .statusCode(200)
                .assertThat()
                .body("authorName.second", equalTo(author.getAuthorName().getSecond()))
                .log();
        log.info("Last name of author with " + author.getAuthorId() + " is " +author.getAuthorName().getSecond());

    }

    @AfterTest
    public void freeResources(){
        authorService.deleteAuthor(author.getAuthorId())
                .then()
                .assertThat()
                .statusCode(204);
        log.info("Delete author with id: " + author.getAuthorId());
    }
}
