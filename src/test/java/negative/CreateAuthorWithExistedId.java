package negative;

import com.epam.ta.rest.model.author.Author;
import com.epam.ta.rest.service.AuthorService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.equalTo;

public class CreateAuthorWithExistedId {
    private static final Logger log = LogManager.getLogger(CreateAuthorWithExistedId.class);
    AuthorService authorService;
    Author author;
    Gson gson;
    String jsonString;

    @BeforeTest
    public void setUp(){
        authorService = new AuthorService();
        author = authorService.createAuthorEntity();
        log.info("Trying to create author: " + author.toString());
        gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        jsonString = gson.toJson(author);
        authorService.createAuthor(jsonString);
        log.info("Created author with ID and last name: "
                + author.getAuthorId()
                + author.getAuthorName().getSecond());
    }

    @Test
    public void createAuthorWithExistedId(){
        authorService.createAuthor(jsonString)
                .then()
                .assertThat()
                .statusCode(409)
                .assertThat()
                .body("error",equalTo("Conflict"))
                .body("errorMessage",equalTo("Author with such 'authorId' already exists!"));
    }

    @AfterTest
    public void freeResource(){
        if (! (authorService.deleteAuthor(author.getAuthorId()).getStatusCode() == 204)) {
            log.info("Author with id " + author.getAuthorId() +" is not deleted");    }
    }
}
