package negative;

import com.epam.ta.rest.constants.Constants;
import io.restassured.response.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class WrongSizeQueryParameter {
    static final Logger log = LogManager.getLogger(WrongSizeQueryParameter.class);

    @Test
    public void wrongSizeQueryParameter(){
        Response response = given()
                .queryParam(Constants.SIZE_QUERY_PARAM,"-1")
                .when()
                .get(Constants.AUTHORS_ENDPOINT);

        if (response.getStatusCode() == 400) {
            response
                    .then()
                    .assertThat()
                    .body("error",equalTo("Bad Request"))
                    .body("errorMessage",equalTo("Value of 'size' parameter must be positive and greater than zero!"));
        }
        else {
            log.info("Response status code must be 400. Received " + response.getStatusCode());
        }
    }
}
