package com.epam.ta.rest.service;

import com.epam.ta.rest.constants.Constants;
import com.epam.ta.rest.model.author.Author;
import com.epam.ta.rest.model.author.AuthorBirthday;
import com.epam.ta.rest.model.author.AuthorName;

import com.github.javafaker.Faker;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.lang.reflect.Type;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;

public class AuthorService {
    public Author createAuthorEntity(){
        Faker faker = new Faker();
        Author newAuthor = new Author();
        List<Author> authorList = getAllAuthors();
        int authorId = getLastAuthorId(authorList) + 1;
        newAuthor.setAuthorId(authorId);
        AuthorName fullName = new AuthorName();
        fullName.setFirst(faker.name().firstName());
        fullName.setSecond(faker.name().lastName());
        newAuthor.setAuthorName(fullName);
        AuthorBirthday birthday = new AuthorBirthday();
        birthday.setCity(faker.address().cityName());
        birthday.setCountry(faker.address().country());
        birthday.setDate(Date.valueOf("1988-06-05"));
        newAuthor.setBirth(birthday);
        newAuthor.setNationality(faker.address().countryCode());
        newAuthor.setAuthorDescription(faker.gameOfThrones().house());
        return newAuthor;
    }
    public List<Author> getAllAuthors(){
        Response response = given()
                .queryParam(Constants.SIZE_QUERY_PARAM,"100")
                .when()
                .get(Constants.AUTHORS_ENDPOINT);

        String jsonBody = response.getBody().asString();
        Type authorsType = new TypeToken<ArrayList<Author>>(){}.getType();
        return new Gson().fromJson(jsonBody, authorsType);
    }
    private int getLastAuthorId(List<Author> authorList){
        return authorList.get(authorList.size()-1).getAuthorId();
    }
    public Response createAuthor(String jsonString) {
        return given()
                .baseUri(Constants.AUTHOR_ENDPOINT)
                .contentType(ContentType.JSON)
                .body(jsonString)
                .when()
                .post();
    }
    public Response deleteAuthor (int authorId){
        return given()
                .baseUri(Constants.AUTHOR_ENDPOINT+authorId)
                .when()
                .delete();
    }
}
