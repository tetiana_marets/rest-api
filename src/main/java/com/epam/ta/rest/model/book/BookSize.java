package com.epam.ta.rest.model.book;

public class BookSize {
    private double height;
    private double width;
    private double length;


    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "BookSize{" +
                "height=" + height +
                ", width=" + width +
                ", length=" + length +
                '}';
    }
}
