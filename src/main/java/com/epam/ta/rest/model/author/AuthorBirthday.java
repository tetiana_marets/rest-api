package com.epam.ta.rest.model.author;

import java.util.Date;

public class AuthorBirthday {
    private String city;
    private String country;
    private java.util.Date date;

    public AuthorBirthday () {
    }
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "AuthorBirthday{" +
                "city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", date=" + date +
                '}';
    }
}
