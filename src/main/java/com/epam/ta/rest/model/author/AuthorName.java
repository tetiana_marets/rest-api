package com.epam.ta.rest.model.author;

public class AuthorName {
    private String first;
    private String second;

    public AuthorName(){
    }

    public String getSecond() {
        return second;
    }

    public void setSecond(String second_name) {
        this.second = second_name;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first_name) {
        this.first = first_name;
    }

    @Override
    public String toString() {
        return "AuthorName{" +
                "first='" + first + '\'' +
                ", second='" + second + '\'' +
                '}';
    }
}
