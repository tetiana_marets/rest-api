package com.epam.ta.rest.model.genre;

public class Genre {
    private int genre_id;
    private String genre_name;
    private String genre_descr;

    public int getGenre_id(){
        return genre_id;
    }

    public void setGenre_id(int genre_id){
        this.genre_id=genre_id;
    }

    public String getGenre_name(){
        return genre_name;
    }

    public void setGenre_name(String genre_name){
        this.genre_name=genre_name;
    }

    public String getGenre_descr(){
        return genre_descr;
    }

    public void setGenre_descr(String genre_descr){
        this.genre_descr=genre_descr;
    }
}
