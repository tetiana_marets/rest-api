package com.epam.ta.rest.model.book;

public class BookAdditionalInformation {
    private int pageCount;
    private BookSize size;

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public BookSize getSize() {
        return size;
    }

    public void setSize(BookSize size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "BookAdditionalInformation{" +
                "pageCount=" + pageCount +
                ", size=" + size +
                '}';
    }
}
