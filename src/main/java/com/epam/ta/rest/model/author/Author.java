package com.epam.ta.rest.model.author;

public class Author implements Comparable<Author>{
    private int authorId;
    private AuthorName authorName;
    private String nationality;
    private AuthorBirthday birth;
    private String authorDescription;

    public Author() {
    }
    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public AuthorName getAuthorName() {
        return authorName;
    }

    public void setAuthorName(AuthorName authorName) {
        this.authorName = authorName;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public AuthorBirthday getBirth() {
        return birth;
    }

    public void setBirth(AuthorBirthday birth) {
        this.birth = birth;
    }

    public String getAuthorDescription() {
        return authorDescription;
    }

    public void setAuthorDescription(String authorDescription) {
        this.authorDescription = authorDescription;
    }

    @Override
    public String toString() {
        return "Author{" +
                "authorId=" + authorId +
                ", authorName=" + authorName +
                ", nationality='" + nationality + '\'' +
                ", birth=" + birth +
                ", authorDescription='" + authorDescription + '\'' +
                '}';
    }

    public int compareTo(Author o) {
        return this.getAuthorId()-o.getAuthorId();
    }
}
