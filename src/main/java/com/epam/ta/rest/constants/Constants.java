package com.epam.ta.rest.constants;

public class Constants {
    public static final String BASE_URI = "http://127.0.0.1:8080";
    public static final String AUTHORS_ENDPOINT = BASE_URI + "/api/library/authors/";
    public static final String AUTHOR_ENDPOINT = BASE_URI + "/api/library/author/";
    public static final String ORDER_BY_QUERY_PARAM = "asc";
    public static final String SIZE_QUERY_PARAM = "size";
    public static final String SORT_BY_QUERY_PARAM = "authorId";
    public static final  String AUTHORS_SEARCH_ENDPOINT = BASE_URI +"api/library/authors/search";
}
